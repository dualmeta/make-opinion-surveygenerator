// TODO: Implement the to-do-item context

import {createContext, Dispatch} from "react";
import {ITodoActions} from "./actions";

export interface ITodo {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}

export interface ITodoContext {
  todo: ITodo;
  todoDispatch: Dispatch<ITodoActions>
}

export const initialTodo: ITodo = {
  userId: 1,
  id: 1,
  title: 'Dummy Todo',
  completed: false,
}

const TodoContext = createContext<ITodoContext>({
  todo: initialTodo,
  todoDispatch: () => {},
});

export default TodoContext;
