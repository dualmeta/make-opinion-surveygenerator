// TODO: Implement the reducer function to store the fetched to-do-item

import {ITodo} from "./context";

interface ISetTodo {
  payload: ITodo;
  type: 'SET_TODO'
}

export const setTodo = (state: ITodo, payload: ISetTodo["payload"]) => (
  {...payload}
);

export type ITodoActions = ISetTodo;
