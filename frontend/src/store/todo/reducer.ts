// TODO: Implement the reducer

import {ITodo} from "./context";
import {ITodoActions, setTodo} from "./actions";

const todoReducer = (state: ITodo, action: ITodoActions): ITodo =>
{
  switch (action.type)
  {
    case "SET_TODO":
    {
      return setTodo(state, action.payload)
    }
  }
};

export default todoReducer;
