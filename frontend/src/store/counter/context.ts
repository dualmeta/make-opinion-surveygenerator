// TODO: Implement the counter context

import React, {Dispatch} from "react";
import {ICounterActions} from "./actions";

export interface ICounter
{
  value: number
}

export const initialCounter: ICounter = {
  value:0
}

export interface ICounterContext {
  counter: ICounter;
  counterDispatch : Dispatch<ICounterActions>;
}

const CounterContext = React.createContext<ICounterContext>({
  counter: initialCounter,
  counterDispatch: () => {},
});

export default CounterContext;
