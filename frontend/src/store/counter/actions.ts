// TODO: Implement the reducer function to increase the counter

import {ICounter} from "./context";

interface IUpdateCounterIncrement {
  type: 'INCREMENT_COUNTER';
}

export const updateCounterIncrement = (state: ICounter): ICounter => ({
  value: state.value + 1
})

interface IUpdateCounterDecrement {
  type: 'DECREMENT_COUNTER';
}

export const updateCounterDecrement = (state: ICounter): ICounter => ({
  value: state.value - 1
})

export type ICounterActions = (
  IUpdateCounterIncrement | IUpdateCounterDecrement
)

