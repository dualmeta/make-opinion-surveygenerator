// TODO: Implement the reducer

import {ICounter} from "./context";
import {ICounterActions, updateCounterDecrement, updateCounterIncrement} from "./actions";

const counterReducer = (state: ICounter, action: ICounterActions): ICounter =>
{
  switch (action.type)
  {
    case "INCREMENT_COUNTER":
    {
      return updateCounterIncrement(state)
    }

    case "DECREMENT_COUNTER":
    {
      return updateCounterDecrement(state)
    }
  }
};

export default counterReducer;
