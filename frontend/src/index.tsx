import React from "react";
import { render } from "react-dom";
import { BrowserRouter } from "react-router-dom";

// import DummyAppWrapper from "./components/dummyPage/dummyAppWrapper/DummyAppWrapper";
import AppWrapper from "./components/appWrapper/AppWrapper";

render(
  <BrowserRouter>
    <AppWrapper/>
  </BrowserRouter>,
  document.getElementById("app")
);
