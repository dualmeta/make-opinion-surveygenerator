import axios, { AxiosResponse } from "axios";

export const fetchTodos = async (): Promise<any /* TODO: Don't use any later */> =>
{
  // TODO: Fetch a dummy to-do from 'https://jsonplaceholder.typicode.com/todos/1' using the 'axios' npm package and return it
  let response: AxiosResponse;

  try {
    response = await axios.get('https://jsonplaceholder.typicode.com/todos/1');
  }
  catch (error) {
    console.log("Fetching Error: ", error);
    return Promise.reject(error);
  }

  console.log("Fetched Todo's successfully: ", response.data);

  return response.data
};
