import { css } from "@emotion/react";

export const wrapper = css`
  hr {
    margin: 2rem 0;
  }
`;
