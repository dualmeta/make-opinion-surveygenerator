import React, {FunctionComponent, useContext} from "react";

import * as styles from "./DummyChildComponent.style";

import CounterContext, {ICounterContext} from "../../../store/counter/context";
import TodoContext, {ITodoContext} from "../../../store/todo/context";

const DummyChildComponent: FunctionComponent = () => {
  /**
   * TODO: Access the provided to-do-item and display it's title, as well as the current counter
   */

  const ctxCounter = useContext<ICounterContext>(CounterContext);
  const ctxTodos = useContext<ITodoContext>(TodoContext);

    return (
    <div css={styles.wrapper}>
      <hr/>
      <h1>This is your To-Do:</h1>
      <div>
        <p>{ctxTodos.todo.title}</p>
      </div>
      <hr/>
      <h1>This is the current counter value:</h1>
      <p>{ctxCounter.counter.value}</p>
      <button onClick={() => {
        ctxCounter.counterDispatch({type: 'INCREMENT_COUNTER'}) /* TODO */
      }}>Increment Counter
      </button>
      <button onClick={() => {
        ctxCounter.counterDispatch({type: 'DECREMENT_COUNTER'}) /* TODO */
      }}>Decrement Counter
      </button>
    </div>
  );
};

export default DummyChildComponent;
