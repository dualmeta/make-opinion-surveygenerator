import React, {FunctionComponent, useEffect, useReducer} from "react";
// import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import DummyChildComponent from "../dummyChildComponent/DummyChildComponent";
import CounterContext, {initialCounter} from "../../../store/counter/context";
import TodoContext, {initialTodo} from "../../../store/todo/context";

import * as styles from "./DummyAppWrapper.style";

import counterReducer from "../../../store/counter/reducer";
import todoReducer from "../../../store/todo/reducer";
import {fetchTodos} from "../../../api/fetchTodo";

const DummyAppWrapper: FunctionComponent = () => {
  /**
   * TODO:
   * As soon as this AppWrapper component mounts, fetch a TO-DO-object from the dummyApi und provide
   * it for all child components using useReducer and the Context API. Also setup a counter starting at 0
   * and provide it for all child components.
   */

  const [counter, counterDispatch] = useReducer(counterReducer, initialCounter);
  const [todo, todoDispatch] = useReducer(todoReducer, initialTodo);

  useEffect(() => {
    fetchTodos()
      .then(todo => {
        todoDispatch({
          payload: todo,
          type: "SET_TODO"
        });
      }).catch(() => {
      // TODO: Implement an error handler showing a Toast with the global state
    });
  }, []);

  return (
    <CounterContext.Provider value={{counter, counterDispatch}}>
      <TodoContext.Provider value={{todo, todoDispatch}}>
        <div css={styles.wrapper}>
          <h1>Hello world!</h1>
          <DummyChildComponent/>
        </div>
      </TodoContext.Provider>
    </CounterContext.Provider>
  );
};

export default DummyAppWrapper;
