import React, { FunctionComponent } from "react";
import { Routes, Route, NavLink } from "react-router-dom";

import { routes } from "../../utils/routes";
import SurveysOverview from "../surveysOverview/SurveysOverview";
import * as styles from "./AppWrapper.style";

const AppWrapper: FunctionComponent = () => (
  <div>
    <NavLink to={routes.base}>
      click to show projects
    </NavLink>
    <div css={styles.wrapper}>
      <Routes>
        <Route path={routes.base} element={<SurveysOverview/>}/>
      </Routes>
    </div>
  </div>
);

export default AppWrapper;
