import { css } from "@emotion/react";

export const wrapper = css`  
  background-color: #ffffff;
  border: solid 1px #e7e7e7;
  padding: 4rem; 
  > h1 {
    font-size: 3rem;
    margin-bottom: 4rem;
  }
`;
