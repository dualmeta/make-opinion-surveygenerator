import React, { FunctionComponent, ComponentProps } from "react";

import surveyLibrary from "../../assets/images/survey_library.png";
import * as styles from "./SurveysOverview.style";

const SurveysOverview: FunctionComponent<ComponentProps<"div">> = () => 
{
  const dummySurveys = [
    {
      date: "23.02.2022", id: "1", name: "Survey 1", status: "not in field"
    },
    {
      date: "24.02.2022", id: "2", name: "Survey 2", status: "not in field"
    },
  ];

  return (
    <div css={styles.wrapper}>
      <div css={styles.header}>
        <img src={surveyLibrary} alt="Survey Library"/>
        <button>New Survey</button>
      </div>
      <div>
        <div>
          <label htmlFor="">
            Show
            <select name="displayQuantity" id="displayQuantity">
              <option value="10">10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="100">100</option>
            </select>
            entries
          </label>
        </div>
      </div>
      <div css={styles.tableHeader}>
        <div>#</div>
        <div>PROJECT ID</div>
        <div>STATUS</div>
        <div>NAME</div>
        <div>CREATED DATE</div>
      </div>
    </div>
  );
};

export default SurveysOverview;
