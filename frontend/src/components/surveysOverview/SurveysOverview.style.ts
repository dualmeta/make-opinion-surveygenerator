import { css } from "@emotion/react";

export const wrapper = css`
  border: black solid;
`;

export const header = css`
  border: black solid;
  display: flex;
  justify-content: space-between;
  align-items: center;
  > img {
    height: 60px;
  }
  > button {
    height: 20px;
  }
`;

export const tableHeader = css`
  border: black solid;
  display: flex;
  justify-content: space-around;
  align-items: center;  
  padding: 2rem;
`;
