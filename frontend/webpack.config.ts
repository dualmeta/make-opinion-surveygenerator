import { Configuration as WebpackConfiguration } from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';

const env = process.env.NODE_ENV === 'production' ? 'production' : 'development';
const port = 3000;

const config: WebpackConfiguration = {
  mode: env,
  entry: {
    app: path.resolve(__dirname, 'src', 'index.tsx'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: '[name].[fullhash].js',
    chunkFilename: 'chunk-[name].[chunkhash].js',
    pathinfo: false,
  },
  cache: {
    type: 'memory',
  },
  devtool: env === 'production' ? false : 'eval-cheap-module-source-map',
  module: {
    rules: [
      {
        test: /\.(jsx|js|ts|tsx)$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          cacheCompression: false,
        },
      },
      {
        test: /\.(gif|png|jpe?g|webp|jfif|ttf|eot|woff|woff2|otf|svg)$/i,
        use: [{
          loader: 'file-loader',
          options: {
            esModule: false,
          },
        }],
      },
    ],
  },
  stats: 'normal',
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'public', 'index.html'),
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'public',
          to: 'dist'
        }
      ]
    }),
    new CleanWebpackPlugin(),
  ],
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  },
  devServer: {
    hot: true,
    historyApiFallback: true,
    port,
    host: '0.0.0.0',
    compress: true,
    client: {
      webSocketURL: {
        hostname: '0.0.0.0',
        pathname: '/ws',
        port,
      },
    },
    devMiddleware: {
      stats: 'errors-only',
    },
  },
};


export default config;
