module.exports = function (api)
{
  api.cache(true);

  return {
    presets: [
      [
        "@babel/preset-react",
        {
          runtime: "automatic",
          importSource: "@emotion/react",
        },
      ],
      "@babel/preset-typescript"
    ],
    plugins: [
      [
        "@emotion",
        {
          sourceMap: true,
          autoLabel: "dev-only",
          cssPropOptimization: true
        }
      ]
    ]
  }
}
